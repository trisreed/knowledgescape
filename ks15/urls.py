from django.conf.urls import patterns, include, url
from django.contrib import admin
from ksapp import views

# removed /generatedJson.json as the .json isnt required
urlpatterns = patterns('',
    url(r'^geom$', views.geomGenerator, name='geomGenerator'),
	url(r'^json$', views.jsonGenerator, name='jsonGenerator'),
    url(r'^$', views.index, name='mainpage'),
)