from django.shortcuts import render, render_to_response
from django import forms
from django.http import HttpResponse
import json
import rdflib
import urllib

# Main form
class SpecifyOntologyForm(forms.Form):
    ontologyName = forms.URLField(label='Ontology URL')
    sparqlQuery = forms.CharField(label='SPARQL Query', required=False, widget=forms.Textarea(attrs={'rows': 4}))

# Generate Geom information
def geomGenerator(request):
    # Check that the URL has been specified in the request
    if ("url" in request.GET) and ("item" in request.GET):
        #try:
            # Try to open the graph
            g = rdflib.Graph()
            g.parse(request.GET["url"], format="application/rdf+xml")

            # SPARQLize out the full URI
            runQuery2 = g.query("""SELECT ?s
                WHERE {
                    ?s ?p ?o .
                }""")

            for rowQ in runQuery2:
                if request.GET['item'] in rowQ[0]:
                    theSubject = rowQ[0]

            # SPARQLize out the geometry
            runQuery3 = g.query("""SELECT ?geom
                WHERE
                {
                  <""" + theSubject + """> geo:polygon ?geom .
                }""")

            returnValue = ""
            for rowR in runQuery3:
                returnValue = rowR[0]

            # Return
            response = HttpResponse()
            response.status_code = 200
            response.write(returnValue)
            return response
        #except:
        #    response = HttpResponse()
        #    response.status_code = 405
        #    response.write("Couldn't open the file.")
        #    return response
    else:
        response = HttpResponse()
        response.status_code = 405
        response.write("You shouldn't be here!")
        return response

# Generate JSON file
def jsonGenerator(request):
    # Check that the URL has been specified in the request
    if "url" in request.GET:
        try:
            # Try to open the graph
            g = rdflib.Graph()
            g.parse(request.GET["url"], format="application/rdf+xml")

            # Create JSON dictionary
            theJson = {}
            theJson['nodes'] = []

            # Create lists for storage of members
            classes = []
            individuals = []

            # Grab classes out of the graph
            for statement in g:
                # Ensure that there is all of s,p,o
                if len(statement) == 3:
                    # Split the info off the reference
                    splitIfClass = statement[2].split('#')
                    # Ensure this worked
                    if len(splitIfClass) == 2:
                        # Check it is a class
                        if "Class" == splitIfClass[1]:
                            # Grab the info off the reference
                            className = statement[0].split('#')[1]
                            # Add into lists
                            classes.append(className)
                            theJson['nodes'].append({'name': className, 'group': 1})

            # Do the same for the individuals
            for statement in g:
                # Ensure all of s,p,o
                if len(statement) == 3:
                    # Split the info off the reference
                    splitClass = statement[2].split('#')
                    # Ensure this worked
                    if len(splitClass) == 2:
                        # Grab it out for comparison
                        theClassItHas = splitClass[1]
                        # Compare with class list
                        if theClassItHas in classes:
                            # Grab individual's name
                            individualName = statement[0].split('#')
                            # Check that this worked
                            if len(individualName) == 2:
                                # Grab it out
                                individualName = individualName[1]
                                # Grab out to compare the other element
                                splitMessage = statement[1].split('#')
                                # Check this worked
                                if len(splitMessage) == 2:
                                    # Do the comparison
                                    if (individualName != theClassItHas) and (individualName not in individuals) \
                                    and (splitMessage[1] != "disjointWith") and (individualName not in classes):
                                        # Add if it worked
                                        individuals.append(individualName)
                                        theJson['nodes'].append({'name': individualName, 'group': 2})

            # Joins, for each statement in the graph, check if a node appears, then mark
            theJson['links'] = []

            # For each statement
            for statement in g:
                # Ensure is has s,p,o
                if len(statement) == 3:
                    theRow = {}
                    # Only interested in s and o
                    firstStatement = statement[0].split('#')
                    thirdStatement = statement[2].split('#')

                    # Only interested in p for disjointness
                    try:
                        secondStatement = statement[1].split('#')
                        secondStatement = secondStatement[1]
                    except:
                        secondStatement = ""

                    # Check they are valid and split
                    if (len(firstStatement) == 2) and (len(thirdStatement) == 2):
                        firstStatement = firstStatement[1]
                        thirdStatement = thirdStatement[1]

                        # Need a counter
                        numberCounter = 0
                        for eachItem in theJson['nodes']:
                            eachItem = eachItem['name']

                            # First put in source, then in target if found
                            if firstStatement == eachItem:
                                theRow['source'] = numberCounter
                            if thirdStatement == eachItem:
                                theRow['target'] = numberCounter

                            numberCounter = numberCounter + 1

                    # Remove false values
                    if ('source' in theRow) and ('target' in theRow):
                        if (theRow['source'] != theRow['target']):
                            if (secondStatement != "disjointWith"):
                                theRow['value'] = 1
                                theJson['links'].append(theRow)

            # Return values including errors
            return HttpResponse(json.dumps(theJson, indent=4), content_type="application/json")
        except:
            response = HttpResponse()
            response.status_code = 405
            response.write("Couldn't open the file.")
            return response
    else:
        response = HttpResponse()
        response.status_code = 405
        response.write("You shouldn't be here!")
        return response

# Generate UI
def index(request):
    # If submitted, display the visualisation. If not, don't.
    if 'submit' not in request.POST:
        form = SpecifyOntologyForm()
        return render_to_response('view.html', {'form': form})
    else:
        form = SpecifyOntologyForm(request.POST)
        if form.is_valid():
            value = form.cleaned_data['ontologyName']
            sparqlValue = form.cleaned_data['sparqlQuery']

            # Run the SPARQL query and output it, if it exists
            if(sparqlValue != ""):
                # Create a list to hold the iterated results
                sparqlResultList = []

                # Try to Run the query
                try:
                    g = rdflib.Graph()
                    g.parse(value, format="application/rdf+xml")
                    runQuery = g.query(sparqlValue)
                except:
                    runQuery = [] # wrong list before!

                # Grab it out row by row and append, then return
                for sparqlRow in runQuery:
                    try:
                        sparqlResultList.append(sparqlRow[0])
                    except:
                        sparqlResultList.append(sparqlRow)

                return render_to_response('view.html', {'form': form, 'displaySubmittedOutput': True, 'scriptValue': value, 'sparqlValue': sparqlResultList})
            else:
                return render_to_response('view.html', {'form': form, 'displaySubmittedOutput': True, 'scriptValue': value})
        else:
            form = SpecifyOntologyForm()
            return render_to_response('view.html', {'form': form})
