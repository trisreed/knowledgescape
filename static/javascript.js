// Map stuff
var map = new L.Map('map');

// create the tile layer with correct attribution
var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
var osm = new L.TileLayer(osmUrl, {minZoom: 4, maxZoom: 16, attribution: osmAttrib});

// start the map in Perth
map.setView(new L.LatLng(-32, 116), 9);

// geolocate
map.locate({setView: true});
map.addLayer(osm);

// polygon needs to be global
var thePoly;

// HTTPRequest shortcut
function httpGet(theUrl, theItem) {
    $.get(
        "/scape/geom",
        {url : theUrl, item : theItem},
        function(data) {
            // remove existing
            try {
                map.removeLayer(thePoly);
            } catch(e) {
                console.log('No current polygons');
            }

            // add new
            try {
                var coordsArray = data.split(" ");
                var index;
                var polygonArray = [];

                // fix for 'returning' polygs
                for (index = 0; index < (coordsArray.length - 1); ++index) {
                    eachCoord = coordsArray[index].split(",");
                    // fix for floats
                    polygonArray.push([parseFloat(eachCoord[1]), parseFloat(eachCoord[0])]);
                }

                // reset the map
                thePoly = new L.polygon(polygonArray).addTo(map);

                // fit to bounds
                map.fitBounds(polygonArray);
            } catch(e) {
                // lol
            }
        }
    );
}

// Graph stuff
var width = 540,
    height = 500;

var color = d3.scale.category20();

var force = d3.layout.force()
    .charge(-120)
    .linkDistance(30)
    .size([width, height]);

var svg = d3.select("#d3").append("svg")
    .attr("width", width)
    .attr("height", height);

d3.json("/scape/json?url=" + scriptName, function(error, graph) {
    force.nodes(graph.nodes)
        .links(graph.links)
        .start();

    var link = svg.selectAll(".link")
        .data(graph.links)
        .enter().append("line")
        .attr("class", "link")
        .style("stroke-width", function (d) { return Math.sqrt(d.value); });

    var node = svg.selectAll(".node")
        .data(graph.nodes)
        .enter().append("g")
        .attr("class", "node")
        .call(force.drag)
        .on('click', function(d,i){
            if (d.group == 2) {
                node.style("fill", "#000000");
                httpGet(scriptName, d.name);
                d3.select(this).style("fill", "#0000ff");
            }
        });

    node.append("circle")
        .attr("r", 8)
        .style("fill", function (d) { return color(d.group); });

    node.append("text")
        .attr("dx", 10)
        .attr("dy", ".35em")
        .text(function(d) { return d.name });

    force.on("tick", function () {
        link.attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });

        d3.selectAll("circle")
            .attr("cx", function (d) { return d.x; })
            .attr("cy", function (d) { return d.y; });

        d3.selectAll("text")
            .attr("x", function (d) { return d.x; })
            .attr("y", function (d) { return d.y; });
    });
});